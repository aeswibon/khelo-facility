package services

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/aeswibon/khelo-facility/generated/pkg/proto/messages"
	"gitlab.com/aeswibon/khelo-facility/generated/pkg/proto/services"
	"gitlab.com/aeswibon/khelo-facility/pkg/db"
	"gitlab.com/aeswibon/khelo-facility/pkg/models"
)

// FacilityServer struct to implement the gRPC server
type FacilityServer struct {
	services.UnimplementedFacilityServiceServer
	H db.Handler
}

// CreateFacility creates a new facility
func (fs *FacilityServer) CreateFacility(ctx context.Context, req *messages.FacilityCreateRequest) (*messages.FacilityCreateResponse, error) {
	facility := &models.Facility{
		Name:        req.Name,
		Description: req.Description,
		Email:       req.Email,
		Phone:       req.Phone,
		Address:     req.Address,
		CityID:      req.City.ExternalId,
		Location: models.Location{
			Latitude:  req.Location.Latitude,
			Longitude: req.Location.Longitude,
		},
		Website:   req.Website,
		OwnerID:   req.OwnerId,
		Rating:    0.0,
		Verified:  false,
		Deleted:   false,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	if err := fs.H.DB.Create(&facility).Error; err != nil {
		return &messages.FacilityCreateResponse{
			Status:  http.StatusConflict,
			Message: err.Error(),
		}, nil
	}
	return &messages.FacilityCreateResponse{
		Status:     http.StatusCreated,
		Message:    "Facility created successfully",
		FacilityId: facility.ExternalID,
	}, nil
}

// GetFacility gets a facility by id
func (fs *FacilityServer) GetFacility(ctx context.Context, req *messages.FacilityGetRequest) (*messages.FacilityGetResponse, error) {
	var facility models.Facility
	if err := fs.H.DB.Where("external_id = ?", req.FacilityId).First(&facility).Error; err != nil {
		return &messages.FacilityGetResponse{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}, nil
	}

	return &messages.FacilityGetResponse{
		Status:  http.StatusOK,
		Message: "Facility found",
		Facility: &messages.Facility{
			ExternalId:  facility.ExternalID,
			Name:        facility.Name,
			Description: facility.Description,
			Email:       facility.Email,
			Phone:       facility.Phone,
			Address:     facility.Address,
			City: &messages.City{
				ExternalId: facility.CityID,
			},
			Location: &messages.Location{
				Latitude:  facility.Location.Latitude,
				Longitude: facility.Location.Longitude,
			},
			Website:  facility.Website,
			OwnerId:  facility.OwnerID,
			Rating:   facility.Rating,
			Verified: facility.Verified,
		},
	}, nil
}

// GetFacilities gets all facilities
func (fs *FacilityServer) GetFacilities(ctx context.Context, req *messages.FacilityListRequest) (*messages.FacilityListResponse, error) {
	var facilities []models.Facility
	if err := fs.H.DB.Find(&facilities).Limit(int(*req.Limit)).Offset(int(*req.Offset)).Error; err != nil {
		return &messages.FacilityListResponse{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}, nil
	}

	var facilityList []*messages.Facility
	for _, facility := range facilities {
		facilityList = append(facilityList, &messages.Facility{
			ExternalId: facility.ExternalID,
			Name:       facility.Name,
			City: &messages.City{
				ExternalId: facility.CityID,
			},
			Location: &messages.Location{
				Latitude:  facility.Location.Latitude,
				Longitude: facility.Location.Longitude,
			},
			Website:  facility.Website,
			OwnerId:  facility.OwnerID,
			Rating:   facility.Rating,
			Verified: facility.Verified,
		})
	}

	return &messages.FacilityListResponse{
		Status:     http.StatusOK,
		Message:    "Facilities found",
		Facilities: facilityList,
	}, nil
}

// UpdateFacility updates a facility
func (fs *FacilityServer) UpdateFacility(ctx context.Context, req *messages.FacilityUpdateRequest) (*messages.FacilityUpdateResponse, error) {
	var facility models.Facility
	if err := fs.H.DB.Where("external_id = ?", req.FacilityId).First(&facility).Error; err != nil {
		return &messages.FacilityUpdateResponse{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}, nil
	}

	facility.Name = req.Name
	facility.Description = req.Description
	facility.Email = req.Email
	facility.Phone = req.Phone
	facility.Website = req.Website
	facility.OwnerID = req.OwnerId
	facility.UpdatedAt = time.Now()

	if err := fs.H.DB.Save(&facility).Error; err != nil {
		return &messages.FacilityUpdateResponse{
			Status:  http.StatusConflict,
			Message: err.Error(),
		}, nil
	}

	return &messages.FacilityUpdateResponse{
		Status:  http.StatusOK,
		Message: "Facility updated successfully",
	}, nil
}

// DeleteFacility deletes a facility
func (fs *FacilityServer) DeleteFacility(ctx context.Context, req *messages.FacilityDeleteRequest) (*messages.FacilityDeleteResponse, error) {
	var facility models.Facility
	if err := fs.H.DB.Where("external_id = ?", req.FacilityId).First(&facility).Error; err != nil {
		return &messages.FacilityDeleteResponse{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}, nil
	}

	facility.Deleted = true

	if err := fs.H.DB.Save(&facility).Error; err != nil {
		return &messages.FacilityDeleteResponse{
			Status:  http.StatusConflict,
			Message: err.Error(),
		}, nil
	}

	return &messages.FacilityDeleteResponse{
		Status:  http.StatusOK,
		Message: "Facility deleted successfully",
	}, nil
}
