package config

import "github.com/spf13/viper"

// Config struct to hold all the config variables
type Config struct {
	Port         string `mapstructure:"PORT"`
	DBURL        string `mapstructure:"DB_URL"`
	JWTSecretKey string `mapstructure:"JWT_SECRET_KEY"`
}

// LoadConfig function to load the config
func LoadConfig() (config Config, err error) {
	viper.SetConfigFile(".env")

	viper.AutomaticEnv()
	err = viper.ReadInConfig()

	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
