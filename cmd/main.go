package main

import (
	"fmt"
	"log"
	"net"

	pb "gitlab.com/aeswibon/khelo-facility/generated/pkg/proto/services"
	"gitlab.com/aeswibon/khelo-facility/pkg/config"
	"gitlab.com/aeswibon/khelo-facility/pkg/db"
	"gitlab.com/aeswibon/khelo-facility/pkg/services"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	c, err := config.LoadConfig()

	if err != nil {
		log.Fatalln("Failed at config", err)
	}

	h := db.Init(c.DBURL)

	lis, err := net.Listen("tcp", c.Port)

	if err != nil {
		log.Fatalln("Failed to listing:", err)
	}

	fmt.Println("Auth Svc on", c.Port)

	s := services.FacilityServer{
		H: h,
	}

	grpcServer := grpc.NewServer()
	pb.RegisterFacilityServiceServer(grpcServer, &s)
	reflection.Register(grpcServer)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalln("Failed to serve:", err)
	}
}
