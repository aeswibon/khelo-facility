package models

import "time"

// Equipment struct defines the equipment model
// type Equipment struct {
// 	ID          int64  `json:"id" gorm:"primaryKey"`
// 	ExternalID  string `json:"external_id"`
// 	Name        string `json:"name"`
// 	Description string `json:"description"`
// 	Quantity    int64  `json:"quantity"`
// }

// FacilityEquipment struct defines the facility equipment model
// type FacilityEquipment struct {
// 	FacilityID  int64 `json:"facility_id" gorm:"primaryKey"`
// 	EquipmentID int64 `json:"equipment_id" gorm:"primaryKey"`
// }

// Reviews struct defines the reviews model
type Reviews struct {
	ID         int64     `json:"id" gorm:"primaryKey"`
	User       string    `json:"user_id"`
	FacilityID string    `json:"facility_id"`
	Comment    string    `json:"comment"`
	Rating     float32   `json:"rating"`
	Deleted    bool      `json:"deleted"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}

// Facility struct defines the facility model
type Facility struct {
	ID          int64    `json:"id" gorm:"primaryKey"`
	ExternalID  string   `json:"external_id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Email       string   `json:"email"`
	Phone       string   `json:"phone"`
	Address     string   `json:"address"`
	CityID      string   `json:"city_id"`
	Location    Location `json:"location"`
	Website     string   `json:"website"`
	// Equipments  []Equipment `json:"equipments" gorm:"many2many:facility_equipments;"`
	Verified  bool      `json:"verified"`
	OwnerID   string    `json:"owner_id"`
	Rating    float32   `json:"rating"`
	ReviewIDs []string  `json:"review_ids" gorm:"type:varchar(64)[]"`
	Deleted   bool      `json:"deleted"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
