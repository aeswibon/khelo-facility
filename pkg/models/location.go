package models

// State struct defines the state model
type State struct {
	ID         int64  `json:"id" gorm:"primaryKey"`
	ExternalID string `json:"external_id"`
	Name       string `json:"name"`
}

// District struct defines the district model
type District struct {
	ID         int64  `json:"id" gorm:"primaryKey"`
	ExternalID string `json:"external_id"`
	Name       string `json:"name"`
	State      State  `json:"state_id" gorm:"foreignKey:ID;references:ID"`
}

// City struct defines the city model
type City struct {
	ID         int64    `json:"id" gorm:"primaryKey"`
	ExternalID string   `json:"external_id"`
	Name       string   `json:"name"`
	District   District `json:"district_id" gorm:"foreignKey:ID;references:ID"`
}

// Location struct defines the location model
type Location struct {
	Latitude  float32 `json:"latitude"`
	Longitude float32 `json:"longitude"`
}
